<?php

require_once '../../vendor/autoload.php';

include '../chunks/header.php';
include '../chunks/body.php';
include '../chunks/footer.php';

/*
$connection = new \App\Main\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$pdo = $connection->getConnection();

$pag = new App\Main\Pagination($pdo, 5);

$pag->getLink();
*/
