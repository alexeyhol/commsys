<?php
require_once '../../vendor/autoload.php';
require_once '../../src/Handlers/mainPageDataExchenge.php';
include 'modalEdit.php';
include 'modalComment.php';
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Написать сообщение</h1>
                <form class="form-group" action="../../src/Handlers/handler.php" method="POST">
                <input type="hidden" name="date" value="<?= date('j-n-Y') ?>">
                <input class="form-control" type="text" name="author" placeholder="Ваше имя"><br>
                <input class="form-control" type="text" name="header" placeholder="Заголовок"><br>
                <textarea class="form-control" type="text" name="short_message" placeholder="Краткое описание"></textarea>
                <textarea class="form-control" name="full_message" placeholder="Текст сообщения"></textarea></<br>
                <button class="btn btn-primary" type="submit">Отправить</button>
            </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-6">
                <?php
                $pag->getLink();
                //$raw->printAll();
                ?>
            </div>
            <div class="col-6">
                <?php
                $com->printComments();
                ?>
            </div>
        </div>
    </div>
</body>
