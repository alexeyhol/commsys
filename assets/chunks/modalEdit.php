<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        
      <div class="modal-header">      
        <h5 class="modal-title" id="exampleModalLongTitle">Edit message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>     
      </div>
        
      <div class="modal-body"> 
            <div class="col-12">
                <form class="form-group" id="edit" action="../../src/Handlers/editMessage.php" method="POST">
                    <input type="hidden" name="date" value="<?= date('j-F-Y') ?>">
                    <input class="form-control" type="text" name="id" placeholder="ID сообщения" required><br>
                    <input class="form-control" type="text" name="header" placeholder="Заголовок"><br>
                    <textarea class="form-control" name="short_message"  placeholder="Краткое описание"></textarea><br>
                    <textarea class="form-control" name="full_message"  placeholder="Введите сообщение"></textarea><br>
                </form>    
            </div>      
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" form="edit" class="btn btn-primary">Изменить</button>
      </div>
    </div>
  </div>
</div>
