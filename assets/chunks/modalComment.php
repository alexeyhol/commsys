<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle1">Ответить</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
      <div class="modal-body">
            <div class="col-12">
                <form class="form-group" id="answer" action="../../src/Handlers/addComment.php" method="POST">
                    <input class="form-control" type="text" name="message_id" placeholder="ID комментируемого сообщения">
                    <input class="form-control" type="text" name="author" placeholder="Ваше имя"><br>
                    <textarea class="form-control" name="comment"  placeholder="Введите сообщение"></textarea><br>
                </form>    
            </div>
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" form="answer" class="btn btn-primary">Комментировать</button>
      </div>
    </div>
  </div>
</div>