<?php

namespace App\Main;

use PDO;

class Pagination
{
    private $pdo;
    
    private $num_page;
    private $per_page;
    private $total;
    
    
    public function __construct($pdo, $per_page)
    {
        $this->pdo = $pdo;
        $this->per_page = $per_page;
        
        $rs = $this->pdo->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM `messages`");
        $rs->execute();
        $rs = $this->pdo->prepare("SELECT FOUND_ROWS()");
        $rs->execute();
        
        $this->total = $rs->fetch(\PDO::FETCH_COLUMN);
        $this->num_page = ceil($this->total / $this->per_page);
    }
   
    public function getLink()
    {
        $current_page = 1;
        
        if (isset($_GET['page']) && $_GET['page'] > 0) {
            $current_page = $_GET['page'];
        }
        
        $start = ($current_page - 1) * $this->per_page;
        

        $query = "SELECT `id`, `header`, `author`, `date`, `short_message`, `full_message` FROM `messages` ORDER BY `id` DESC LIMIT ?,?" ;
        $stmt = $this->pdo->prepare($query);
        $stmt ->execute([$start, $this->per_page]);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
        $page = 0;
       
        for ($i = 0; $i < count($data); $i++) {
            echo <<<EOT
            <div>
            <b>ID: </b>{$data[$i]['id']}<br>
            <b>Заголовок: </b>{$data[$i]['header']}<br>
            <b>Короткое описание: </b>{$data[$i]['short_message']}<br>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Редактировать</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter1">Ответить</button>
            <hr>
            </div>
            EOT;
        }
    
        while ($page++ < $this->num_page) {
            if ($page == $current_page) {
                echo "<b>{$page}</b>";
            } else {
                echo "<a class=\"pagin\" href=\"?page={$page}\">{$page}</a>";
            }
        }
    }
}
