<?php

/* Получение данных из формы  */

namespace App\Main;

class GetPostData
{
    private $id;
    private $header;
    private $author;
    private $date;
    private $short_message;
    private $full_message;
    
    
    public function __construct()
    {
        foreach ($_POST as $key => $value) {
            switch ($key) {
                case 'id': $this->id = $value;
                    break;
                case 'header': $this->header = $value;
                    break;
                case 'author': $this->author = $value;
                    break;
                case 'date': $this->date = $value;
                    break;
                case 'short_message': $this->short_message = $value;
                    break;
                case 'full_message': $this->full_message = $value;
                    break;
            }
        }
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getHeader()
    {
        return $this->header;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }

    public function getDate()
    {
        return $this->date;
    }
    
    public function getFullMessage()
    {
        return $this->full_message;
    }

    public function getShortMessage()
    {
        return $this->short_message;
    }
}
