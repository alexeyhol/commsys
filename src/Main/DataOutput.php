<?php

/* Вывод данных из базы  */

namespace App\Main;

class DataOutput
{
    private $raw;
    
    private $id;
    private $header;
    private $author;
    private $date;
    private $short_message;
    private $full_message;
    
    private $message_id;
    private $comment;
    
    public function __construct($data)
    {
        $this->raw = $data;
        
        foreach ($data as $v) {
            foreach ($v as $key => $value) {
                switch ($key) {
                case 'id': $this->id  = $value;
                    break;
                case 'header': $this->header = $value;
                    break;
                case 'author': $this->author = $value;
                    break;
                case 'date': $this->date = $value;
                    break;
                case 'short_message': $this->short_message = $value;
                    break;
                case 'full_message': $this->full_message = $value;
                    break;
                case 'message_id': $this->message_id = $value;
                    break;
                case 'comment': $this->comment = $value;
            }
            }
        }
    }
    
    public function getRaw()
    {
        return $this->raw;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getHeader()
    {
        return $this->header;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function getShortMessage()
    {
        return $this->short_message;
    }
    
    public function getFullMessage()
    {
        return $this->full_message;
    }
    
    public function getMessageId()
    {
        return $this->message_id;
    }
    
    public function getComment()
    {
        return $this->comment;
    }
    
    public function printAll()
    {
        for ($i = 0; $i < count($this->raw); $i++) {
            
            /*<b>Автор: </b>{$this->raw[$i]['author']}<br>
             *<b>Дата: </b>{$this->raw[$i]['date']}<br>
             *<b>Полное сообщение: </b>{$this->raw[$i]['full_message']}<br>
             */
            
            echo <<<EOT
            <div>
            <b>ID: </b>{$this->raw[$i]['id']}<br>
            <b>Заголовок: </b>{$this->raw[$i]['header']}<br>
            <b>Короткое описание: </b>{$this->raw[$i]['short_message']}<br>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Редактировать</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter1">Ответить</button>
            <hr>
            </div>
            EOT;
        }
    }
    
    public function printComments()
    {
        for ($i = 0; $i < count($this->raw); $i++) {
            echo <<<EOL
            <div>
            <b>ID комментируемого сообщения: </b>{$this->raw[$i]['message_id']}<br>
            <b>Автор: </b>{$this->raw[$i]['author']}<br>
            <b>Комментарий: </b>{$this->raw[$i]['comment']}<br>
            <hr>
            </div>
            EOL;
        }
    }
}
