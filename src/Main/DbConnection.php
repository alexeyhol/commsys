<?php

/*
 * Класс для соединения с базой данных, а так же для обработки запросов к базе
 */
namespace App\Main;

use \PDO;

class DbConnection
{
    protected $connection;
    
    private const OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
    ];
    
    public function __construct($dbdriver, $host, $dbname, $charset, $username, $passwd)
    {
        $dsn = "$dbdriver:host=$host;dbname=$dbname;charset=$charset";
        $this->connection = new PDO($dsn, $username, $passwd, self::OPTIONS);
    }
    
    public function getConnection()
    {
        return $this->connection;
    }
    
    
    /* Получение данных из базы (сообщения) */
    
    public function select()
    {
        $query = "SELECT `id`, `header`, `author`, `date`, `short_message`, `full_message` FROM `messages` ORDER BY `id` DESC" ;
        $stmt = $this->connection->prepare($query);
        $stmt ->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    
    /* Добавление данных в базу (сообщения) */

    public function add($header, $author, $date, $short_message, $full_message)
    {
        $query = "INSERT INTO `messages`(`header`, `author`, `date`, `short_message`, `full_message`) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->connection->prepare($query);
        $stmt ->execute([$header, $author, $date, $short_message, $full_message]);
    }

    /* Изменение данных в базе (сообщения) */
    
    public function update($header, $short_message, $full_message, $id)
    {
        $query = "UPDATE `messages` SET `header` = ?, `short_message` = ?, `full_message` = ? WHERE `id` = ?";
        $stmt = $this->connection->prepare($query);
        $stmt ->execute([$header, $short_message, $full_message, $id]);
    }
    
    /* Добавление данных в базу (комментарии) */
    
    public function addComment($message_id, $author, $comment)
    {
        $query = "INSERT INTO `comments` (`message_id`,`author`, `comment`) VALUES (?, ?, ?)";
        $stmt = $this->connection->prepare($query);
        $stmt ->execute([$message_id, $author, $comment]);
    }
    
    
    /* Получение данных из базы (комментарии) */
    
    public function getComment()
    {
        $query = "SELECT `message_id`, `author`, `comment` FROM `comments`";
        $stmt = $this->connection->prepare($query);
        $stmt ->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}
