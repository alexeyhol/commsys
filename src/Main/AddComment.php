<?php

namespace App\Main;

class AddComment
{
    private $message_id;
    private $author;
    private $comment;
    
    public function __construct()
    {
        foreach ($_POST as $key => $value) {
            switch ($key) {
                case 'message_id': $this->message_id = $value;
                    break;
                case 'author': $this->author = $value;
                    break;
                case 'comment': $this->comment = $value;
                    break;
            }
        }
    }
    
    public function getMessageId()
    {
        return $this->message_id;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }

    public function getComment()
    {
        return $this->comment;
    }
}
