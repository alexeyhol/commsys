<?php

require_once  '../../vendor/autoload.php';

$connection = new \App\Main\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$data = new \App\Main\GetPostData();

$connection->add($data->getHeader(), $data->getAuthor(), $data->getDate(), $data->getShortMessage(), $data->getFullMessage());

header("Location: http://localhost/commsys/assets/templates/homepage.php");
