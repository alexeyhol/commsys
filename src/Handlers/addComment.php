<?php

require_once  '../../vendor/autoload.php';

$connection = new \App\Main\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$data = new App\Main\AddComment();

$connection->addComment($data->getMessageId(), $data->getAuthor(), $data->getComment());

header("Location: http://localhost/commsys/assets/templates/homepage.php");
