<?php

$connection = new \App\Main\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$pdo = $connection->getConnection();

$data = $connection->select();

$raw = new App\Main\DataOutput($data);

$pag = new App\Main\Pagination($pdo, 5);

$comments = $connection->getComment($raw->getId());

foreach ($comments as $key => $value) {
    $comments = $connection->getComment($raw->getId());
}

$com = new \App\Main\DataOutput($comments);
