<?php

require_once  '../../vendor/autoload.php';

$connection = new \App\Main\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$data = new \App\Main\GetPostData();

$connection->update($data->getHeader(), $data->getShortMessage(), $data->getFullMessage(), $data->getId());

header("Location: http://localhost/commsys/assets/templates/homepage.php");
